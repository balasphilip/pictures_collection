'use strict';

var app = angular.module('piCollApp', [
	'ngResource',
	'ngSanitize',
	'ngRoute',
	'ui.router',
	'LocalStorageModule',
	'ngSanitize'
]);

/* described routes*/
app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

//    $locationProvider.html5Mode(true);

	$urlRouterProvider.otherwise("/");

	$stateProvider
		.state('hello', {
			url: "/hello",
			templateUrl: "views/main.html",
			controller: 'MainCtrl'
		})
});

app.run(function ($rootScope, localStorageService, $state) {


});
